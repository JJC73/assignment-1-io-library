%define EXIT 60
%define NUMBER_SYSTEM 10
%define READ_SYSCALL 1
%define SPACE 0x20
%define TAB 0x9
%define LF 0xA
; Принимает код возврата и завершает текущий процесс
exit:
    xor rax, rax,
    mov rax, EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        mov cl, [rdi+rax]
        test cl, cl
        jz .exit
        inc rax
        jmp .loop
    .exit:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length
    mov rsi, rdi
    mov rdx, rax
    mov rdi, 1
    mov rax, 1
    syscall

    ret

; Принимает код символа и выводит его в stdout
print_char:

    xor rax, rax
    push rdi
    mov rsi , rsp
    mov rdx, 1
    mov rdi, 1
    mov rax, 1
    syscall
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n'
   call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r8, NUMBER_SYSTEM
    mov r9, rsp
    push 0
    .loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        jz .end
        jmp .loop
    .end:
        mov rdi, rsp
        call print_string
        mov rsp, r9
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax, rax
    cmp rdi, 0
    jge .pos

    .neg:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
    .pos:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx
    .loop:
        mov r9b, [rdi+rcx]
        mov r10b, [rsi+rcx]
        cmp r9b, 0
        jz .isfalse
        cmp r10b, 0
        jz .isfalse
        cmp r9b, r10b
        jnz .isfalse
        inc rcx
        jmp .loop
    .isfalse:
        cmp r9b, r10b
        jz .istrue
        mov rax, 0
        ret
    .istrue:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push 0
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
;rdi - buff
;rsi - maxlen
        xor rax, rax
        xor rcx, rcx
        .loop:
                push rdi
                push rsi
                push rcx
                call read_char
                pop rcx
                pop rsi
                pop rdi

                cmp rax, 0
                jz .end

                cmp rax, SPACE
                jz .space_char
                cmp rax, TAB
                jz .space_char
                cmp rax, LF
                jz .space_char

                cmp rcx, rsi
                jge .maxlen


                mov [rdi+rcx], rax
                inc rcx

                jmp .loop
        .space_char:
                cmp rcx, 0
                jz .loop
                jmp .end

        .maxlen:
                xor rax, rax
                xor rdx, rdx
                ret

        .end:
                mov byte[rdi+rcx], 0
                mov rax, rdi
                mov rdx, rcx
                ret



; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r9, 10
    .loop:
        xor rdx, rdx
         mov r8b, byte[rdi+rcx]
         cmp r8b, 0
         je .end
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end
         sub r8b, '0'
        mul r9
        mov dl, r8b
        add rax, rdx
        inc rcx
        jmp .loop
    .end:
        mov rdx, rcx
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    cmp byte[rdi], '-'
    jz .neg
    .pos:
        call parse_uint
        ret
    .neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
        cmp rcx, rdx
        jz .maxlen
        mov r9b, byte[rdi+rcx]
        mov byte[rsi+rcx], r9b
        cmp r9b, 0
        inc rcx
        jz .end
        jmp .loop
    .maxlen:
        mov rax, 0
        ret
    .end:
        mov rax, rcx
        ret
